package com.control.back.halo.stock.notice;

import com.control.back.halo.stock.notice.enu.SenderEnum;
import com.control.back.halo.stock.notice.impl.MailSender;

public class SenderFactory {

    @SuppressWarnings("unchecked")
    public <T, M> Sender<T, M> produce(SenderEnum senum) {
        Sender<T, M> sender = null;
        switch (senum) {
        case MAIL:
            sender = (Sender<T, M>) new MailSender();
            break;
        default:
            break;
        }
        return sender;
    };
}
