package com.control.back.halo.stock.service;

import com.control.back.halo.stock.cache.StockCache;
import com.control.back.halo.stock.data.entity.RegStockInfo;

public class RegStockService extends StockCache<RegStockInfo> {

    private static String          REG_STOCK_CACHE_NAME = "reg_scache";

    private static RegStockService rss;

    private RegStockService() {
        setCacheName(REG_STOCK_CACHE_NAME);
    }

    public static RegStockService getInstance() {
        if (rss == null) {
            rss = new RegStockService();
        }
        return rss;
    }
}
