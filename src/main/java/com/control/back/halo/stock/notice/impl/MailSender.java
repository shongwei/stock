package com.control.back.halo.stock.notice.impl;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.stock.notice.Sender;

@ConfigurationProperties(prefix = "halo.sender")
public class MailSender implements Sender<String, String> {

    private static Logger logger = Logger.getLogger(MailSender.class);

    @Override
    public boolean send(String toSender, String message) {

        Properties props = new Properties(); // 可以加载一个配置文件
        // 使用smtp：简单邮件传输协议
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", this.getMailSmtpHost());// 存储发送邮件服务器的信息
        props.put("mail.smtp.auth", this.getMailSmtpAuth());// 同时通过验证

        Session session = Session.getInstance(props);// 根据属性新建一个邮件会话
        // session.setDebug(true); //有他会打印一些调试信息。

        try {
            MimeMessage mailMessage = new MimeMessage(session);// 由邮件会话新建一个消息对象
            mailMessage.setFrom(new InternetAddress(this.getMailFromSender()));// 设置发件人的地址
            mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(toSender));// 设置收件人,并设置其接收类型为TO
            mailMessage.setSubject(this.getMailTitle());// 设置标题
            // 设置信件内容
            // message.setText(mailContent); //发送 纯文本 邮件 todo
            mailMessage.setContent(message, "text/html;charset=utf-8"); // 发送HTML邮件，内容样式比较丰富
            mailMessage.setSentDate(new Date());// 设置发信时间
            mailMessage.saveChanges();// 存储邮件信息

            // 发送邮件
            // Transport transport = session.getTransport("smtp");
            Transport transport = session.getTransport();
            transport.connect(this.getMailSenderUsername(), this.getMailSenderPassword());
            transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
            // 发送邮件,其中第二个参数是所有已设好的收件人地址
            transport.close();
        } catch (MessagingException e) {
            logger.error("send mail error.params[toSender:{},message:{}]", toSender, message, e);
        }

        return false;
    }
    
    private String        mailSmtpHost;
    private String        mailSmtpAuth;
    private String        mailFromSender;
    private String        mailTitle;
    private String        mailSenderUsername;
    private String        mailSenderPassword;

    public String getMailSmtpHost() {
        return mailSmtpHost;
    }

    public void setMailSmtpHost(String mailSmtpHost) {
        this.mailSmtpHost = mailSmtpHost;
    }

    public String getMailSmtpAuth() {
        return mailSmtpAuth;
    }

    public void setMailSmtpAuth(String mailSmtpAuth) {
        this.mailSmtpAuth = mailSmtpAuth;
    }

    public String getMailFromSender() {
        return mailFromSender;
    }

    public void setMailFromSender(String mailFromSender) {
        this.mailFromSender = mailFromSender;
    }

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle;
    }

    public String getMailSenderUsername() {
        return mailSenderUsername;
    }

    public void setMailSenderUsername(String mailSenderUsername) {
        this.mailSenderUsername = mailSenderUsername;
    }

    public String getMailSenderPassword() {
        return mailSenderPassword;
    }

    public void setMailSenderPassword(String mailSenderPassword) {
        this.mailSenderPassword = mailSenderPassword;
    }

}
