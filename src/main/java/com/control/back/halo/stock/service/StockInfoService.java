package com.control.back.halo.stock.service;

import java.util.List;

import com.control.back.halo.stock.bundle.ConfigBundle;
import com.control.back.halo.stock.cache.StockCache;
import com.control.back.halo.stock.data.StockInfoStorage;
import com.control.back.halo.stock.data.entity.StockInfo;
import com.control.back.halo.stock.data.file.StockOprInFile;
import com.control.back.halo.stock.notice.MsgNotice;

/**
 * 
 * 
 * @author zhaohuiliang
 * @see StockInfoStorage#stockDataPersistence()
 */
@Deprecated
public class StockInfoService extends StockCache<StockInfo> implements MsgNotice<String, StockInfo> {

    private static String           STOCK_INFO_CACHE_NAME = "s_info_cache";

    private StockOprInFile          stockOpfile;

    private static StockInfoService rss;

    private StockInfoService() {
        setCacheName(STOCK_INFO_CACHE_NAME);
        stockOpfile = StockOprInFile.getInstance();
        stockOpfile.setAllStockfileAddress(ConfigBundle.getString("buy_stock_fileAddress"));
    }

    public static StockInfoService getInstance() {
        if (rss == null) {
            rss = new StockInfoService();
        }
        return rss;
    }

    @Override
    public void insert(StockInfo v) {
        super.insert(v);
    }

    public void insertToFile() {
        List<StockInfo> infos = queryAll();
        stockOpfile.insertStock(infos);
    }

    public void insertToFile(List<StockInfo> infos) {
        stockOpfile.insertStock(infos);
    }

    @Override
    public void printMsg() {

    }

}
