package com.control.back.halo.stock.data.database;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.control.back.halo.stock.data.StockInfoStorage;
import com.control.back.halo.stock.data.database.dao.StockInDatabase;
import com.control.back.halo.stock.data.entity.StockInfo;

@Service("stockDatabaseStorage")
public class StockInDatabaseStorage extends StockInfoStorage {

    @Autowired
    private StockInDatabase stockInDatabase;

    @Override
    public List<StockInfo> queryAll() {
        return stockInDatabase.findAll();
    }

    public long count() {
        return stockInDatabase.count();
    }

    @Override
    public void insertStock(List<StockInfo> stcoks) {
        for (Iterator<StockInfo> iterator = stcoks.iterator(); iterator.hasNext();) {
            StockInfo stockInfo = iterator.next();
            StockInfo stock = stockInDatabase.queryByStockCode(stockInfo.getStockCode());
            if (stock != null) {
                stockInfo.setId(stock.getId());
            }
        }
        stockInDatabase.save(stcoks);
    }

}
