package com.control.back.halo.stock.notice;

public interface Sender<T, M> {
    public boolean send(T toSender, M message);
}
