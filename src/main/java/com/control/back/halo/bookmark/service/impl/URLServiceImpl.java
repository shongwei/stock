package com.control.back.halo.bookmark.service.impl;

import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.bookmark.service.IURLService;

@Service
public class URLServiceImpl implements IURLService {

    private static Logger logger = Logger.getLogger(URLServiceImpl.class);

    @Override
    public boolean exists(String URLName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            logger.error("exists [urladdress:%s]", e, URLName);
            return false;
        }
    }
}