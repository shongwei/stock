package com.control.back.halo.bookmark.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.control.back.halo.basic.entity.BaseEntity;

@Entity
@Table(name = "halo_category")
public class Category extends BaseEntity {

    /**
     */
    private static final long serialVersionUID = -5123091774594274385L;

    /**
     * name,parent_id,add_date,last_modifed
     */

    String title;

    Long dateAdded;
    Long dateGroupModified;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    Category parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    List<Category> children;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    List<Mark> marks;

    @Column
    private Integer titleHashCode;

    public Integer getTitleHashCode() {
        return titleHashCode;
    }

    public void setTitleHashCode(Integer titleHashCode) {
        this.titleHashCode = titleHashCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Long getDateGroupModified() {
        return dateGroupModified;
    }

    public void setDateGroupModified(Long dateGroupModified) {
        this.dateGroupModified = dateGroupModified;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    @JSONField(serialize = false)
    public Category getParent() {
        return parent;
    }

    public String getParentTitle() {
        if (parent != null) {
            return parent.getTitle();
        }
        return null;
    }

    @JSONField(serialize = false)
    public List<Category> getChildren() {
        return children;
    }

    @JSONField(serialize = false)
    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (getClass() != obj.getClass())
            return false;
        Category other = (Category) obj;
        if (parent != null && other.parent != null) {
            if (!StringUtils.equals(parent.getTitle(), other.parent.getTitle())) {
                return false;
            }
        } else if (other.parent == null && parent == null) {
        } else {
            return false;
        }

        if (!StringUtils.equals(title, other.title)) {
            return false;
        }
        return true;
    }
}