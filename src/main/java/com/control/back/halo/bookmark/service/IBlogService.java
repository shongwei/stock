package com.control.back.halo.bookmark.service;

import com.control.back.halo.bookmark.entity.Article;
import com.control.back.halo.basic.service.IBaseService;

public interface IBlogService extends IBaseService<Article, Long> {

	/**
	 * 通过url获取文章属性
	 * 
	 * @param url
	 * @return
	 */
	public Article getArticle(String url);

}
