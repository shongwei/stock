package com.control.back.halo.bookmark.json;

public class Response {

    private Boolean incomplete_results;
    private String error;
    private String message;

    private Integer total_count;

    private Object items;

    public Response() {
    }

    public Response(Boolean incomplete_results, String message, Object item) {
        setIncomplete_results(incomplete_results);
        setMessage(message);
        setItems(item);
    }

    public static Response error(String message) {
        Response r = new Response(false, message, null);
        r.setError(message);
        return r;
    }

    public static Response incomplete_results(String message) {
        Response r = new Response(true, message, null);
        return r;
    }

    public Integer getObjectotal_count() {
        return total_count;
    }

    public void setObjectotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public void setItems(Object items) {
        this.items = items;
    }

    public Object getItems() {
        return items;
    }

    public Boolean getIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(Boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}