package com.control.back.halo.bookmark.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.entity.User;
import com.control.back.halo.bookmark.entity.Mark;

@Repository(value = "markDaoImpl")
public interface IMarkDao extends IBaseDao<Mark, Long> {

    @Query("from Mark c where c.createdBy = ?1")
    public List<Mark> findAllByAuth(User user);

    @Query("from Mark u where u.category.id in ?1")
    public List<Mark> findByCategoryIds(List<Long> categoryIds);

    @Query(nativeQuery = true, value = "SELECT * FROM halo_mark where created_by_id = ?1 and href_hash_code in ( SELECT href_hash_code FROM halo_mark where created_by_id = ?1 group by href_hash_code having count(1) > 1)  order by href_hash_code")
    List<Mark> findRepeatMark(Long userId);
}
