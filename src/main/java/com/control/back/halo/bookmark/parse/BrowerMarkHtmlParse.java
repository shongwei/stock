
package com.control.back.halo.bookmark.parse;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.bookmark.entity.Category;
import com.control.back.halo.bookmark.entity.Mark;
import com.control.back.halo.bookmark.service.ICategoryService;
import com.control.back.halo.bookmark.service.IMarkService;

/**
 * 网页书签解析<br>
 * 网页书签解析
 *
 * @author zhaohuiliang
 * @see Jsoup
 * @since 20151019
 * 
 */
@Component
public class BrowerMarkHtmlParse {

	private static Logger logger = Logger.getLogger(BrowerMarkHtmlParse.class);

	private List<Category> categorys = new ArrayList<Category>();

	private List<Mark> marks = new ArrayList<Mark>();

	private List<Category> oldCategorys = null;

	private List<Mark> oldMarks = null;

	@Autowired
	private ICategoryService categoryDao;

	@Autowired
	private IMarkService markDao;

	public void clear() {
		categorys.clear();
		marks.clear();
		oldCategorys.clear();
		oldMarks.clear();
	}

	public void loadCategoryAndMarks() {
		if (oldCategorys == null || oldCategorys.isEmpty()) {
			oldCategorys = categoryDao.findAll();
		}
		if (oldMarks == null || oldMarks.isEmpty()) {
			oldMarks = markDao.findAll();
		}
	}

	public void analysisBrowerMarkHtml(InputStream is) throws IOException {
		loadCategoryAndMarks();
		Document browerMarkhtm = Jsoup.parse(is, "UTF-8", "http://example.com/");
		Element body = browerMarkhtm.body();
		body.select("p").remove();
		body.select("h1").remove();
		retryEle(body);
	};

	public void retryEle(Element body) {
		Elements childs = body.children();
		if (childs != null) {
			if (childs.size() == 1) {
				retryEle(childs.get(0));
			} else {
				Category category = new Category();
				String title = "WDSQ_SYSTEM";
				category.setTitle(title);
				category.setDateAdded(System.currentTimeMillis());
				category.setDateGroupModified(System.currentTimeMillis());
				category.setTitleHashCode(title.toLowerCase().hashCode());
				if (oldCategorys != null) {
					int index = oldCategorys.indexOf(category);
					if (index >= 0) {
						category = oldCategorys.get(index);
					}
				}
				parseChildNode(body, category);
			}
		}
	}

	/**
	 * 
	 * 根节点验证解析方式 <br>
	 * Elements eles = ele.select("dt > dl > dt > a[href]");
	 * 
	 * @param ele
	 * @param parent
	 */
	public void parseChildNode(Element ele, Category parent) {
		for (Element element : ele.children()) {
			Elements dldt = element.select("dt > h3");
			if ((dldt != null && !dldt.isEmpty())) {
				Element child0 = element.child(0);
				String html = child0.outerHtml();
				Category category = new Category();
				if (html.startsWith("<h3") && html.endsWith("</h3>")) {
					logger.debug("folder:" + child0.text() + ",folder:" + parent.getTitle());
					category.setDateAdded(Long.parseLong(child0.attr("ADD_DATE")));
					String text = child0.text();
					if (StringUtils.equals(child0.text().trim(), "书签栏")
							|| StringUtils.equals(child0.text().trim(), "收藏栏")
							|| StringUtils.equals(child0.text().trim(), "Links")) {
						text = "收藏夹";
					}

					category.setTitle(text);
					category.setTitleHashCode(text.toLowerCase().hashCode());
					category.setDateGroupModified(StringUtil.isBlank(child0.attr("LAST_MODIFIED"))
							? (new Date().getTime()) : Long.parseLong(child0.attr("LAST_MODIFIED")));
					category.setParent(parent);
					if (oldCategorys != null) {
						int index = oldCategorys.indexOf(category);
						if (index >= 0) {
							category = oldCategorys.get(index);
						}
					}
					categorys.add(category);
				}
				if (category.getParent() == null) {
					category = parent;
				}
				parseChildNode(element, category);
			} else {
				printDtaHref(element, parent);
			}
		}
	}

	/**
	 *
	 * @param element2
	 * @param parent
	 */
	public void printDtaHref(Element element2, Category parent) {
		Elements dtas = element2.select("dt > a[href]");
		if (dtas != null && !dtas.isEmpty()) {
			for (Element element : dtas) {
				logger.debug("text:" + element.text() + ",pid:" + parent.getTitle());
				Mark mark = new Mark();
				mark.setAddDate(element.attr("ADD_DATE"));
				mark.setName(element.text());
				mark.setCategory(parent);
				String href = element.attr("HREF");
				mark.setHref(href);
				mark.setIcon(element.attr("ICON"));
				mark.setLastModifed(StringUtil.isBlank(element.attr("LAST_MODIFIED")) ? (new Date().getTime() + "")
						: element.attr("LAST_MODIFIED"));
				mark.setHrefHashCode(href != null ? href.toLowerCase().hashCode() : 0);
				if (!oldMarks.contains(mark)) {
					marks.add(mark);
				}
			}

		}
	}

	/**
	 * 获取分类信息
	 * 
	 * @return
	 */
	public List<Category> getCategorys() {
		return categorys;
	}

	/**
	 * 获取书签信息
	 * 
	 * @return
	 */
	public List<Mark> getMarks() {
		return marks;
	}
}