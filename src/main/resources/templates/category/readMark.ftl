<html lang="en" class="width-wide font-sentinel mode-night size-medium" dir="ltr">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>${(article.title)!''}</title>
		<link href="${baseurl}/resources/css/article.css" rel="stylesheet">
	</head>

	<body class="article" data-user-authed="1" youdao="bind">
		<div class="container  bookmark">
			<article class="hentry">
				<header role="banner">
					<div class="entry-origin">
						<a href="${(article.url)!''}" target="_blank">
							${(article.url)!''}
						</a>
					</div>
					<h1 class="entry-title">${(article.title)!''}</h1>
					<ul class="entry-meta">
						<li>
						<#if article.publishDate??>
							<time class="updated" datetime="${(article.publishDate?string("yyyy-MM-dd HH:mm"))!''}" pubdate="">${article.publishDate?string("yyyy-MM-dd HH:mm")}</time>
						</#if>
						</li>
						<li>
							<a href="${article.url}" target="_blank">
								original
							</a>
						</li>
					</ul>
				</header>
				<section role="main">
					<span id="scroll-bullet">?</span>
					<div class="entry-content">
						<div>
							<div class="BlogContent">
								${article.content}
							</div>
						</div>
					</div>
					<section id="print-url">
						<h1>Original URL:</h1>
						<p>${article.url}</p>
					</section>
				</section>
			</article>
	</body>
</html>