<div class="modal-dialog" style="width:90% !important;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">重复的书签</h4>
		</div>
		<div class="modal-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="25%">名称</th>
						<th width="15%">所属分类</th>
						<th colspan="2" width="20%" style="text-align: center;">操作</th>
					</tr>
				</thead>
				<tbody>
					<#if repeatMarks??>
						<#list repeatMarks?keys as key>
							<#if repeatMarks[key]?size gt 1>
								<#list repeatMarks[key] as mark>
									<tr id="${key}_${mark_index}">
										<td>
											<a href="${mark.href}" target="_blank">${mark.name}</a>
										</td>
										<td>${mark.categoryName}</td>
										<td><input type="radio" name="${key}_radio" value="${mark.id}" /></td>
										<#if mark_index==0>
											<td rowspan="${repeatMarks[key]?size}" style="text-align: center;vertical-align:middle;">
												<button class="btn btn-info" type="button" onclick="removeM('${key}_radio',this,'${key}',${repeatMarks[key]?size});">保留</button>
											</td>
										</#if>
									</tr>
								</#list>
								<tr id="${key}_null">
									<td colspan="4"></td>
								</tr>
							</#if>
						</#list>
						<#else>
							<tr>
								<td colspan="4">未查询到结果</td>
							</tr>
					</#if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer ">
			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>