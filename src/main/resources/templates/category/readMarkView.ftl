<div class="modal-dialog" style="width:99% !important;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">${article.title}</h4>
		</div>
		<div class="modal-body">
			<iframe style="width: 100%;height: 100%;">
				<html lang="en" class="width-wide font-sentinel mode-night size-medium" dir="ltr">
					<head>
					    <meta charset="utf-8">
					    <meta name="viewport" content="width=device-width, initial-scale=1">
					    <title>${article.title}</title>
						<link rel="stylesheet" href="./article.css">
					</head>	
					<body class="article" data-user-authed="1" youdao="bind">
					    
					<div class="container  bookmark">
					    <article class="hentry" >
					        <header role="banner">
					            <div class="entry-origin">
					                <a href="${article.url}" id="article-url" title="View the original article" class="original-source" data-stat="rv-banner-original">
					                    ${article.url}
					                </a>
					            </div>
					            <h1 class="entry-title">${article.title}</h1>
					        </header>
					
					        <section role="main">
					            <span id="scroll-bullet">?</span>
					
					            <div class="entry-content">
					                <div>
										<div class="BlogContent">
											${article.content}
										</div>
									</div>
					            </div>
					            <section id="print-url">
					                <h1>Original URL:</h1>
					                <p>${article.url}</p>
					            </section>
					        </section>
					    </article>
					</body>
				</html>
			</iframe>
		</div>
	</div>
</div>