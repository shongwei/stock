<div class="modal fade" id="importMark" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="width:70% !important;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">从html导入书签</h4>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data">
					<input id="file-0a" name="file" class="file" type="file" />
					<div id="kv-error-1" class="alert alert-warning fade in" style="margin-top:10px;display:none"></div>
					<div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
