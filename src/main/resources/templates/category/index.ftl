<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<title> halo bookmark</title>
<link href="/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="/css/animate.css" rel="stylesheet">
<link href="/css/style.css?v=4.1.0" rel="stylesheet">
<link href="//cdn.bootcss.com/zTree.v3/3.5.24/css/zTreeStyle/zTreeStyle.css" rel="stylesheet">
<link href="/js/plugins/bootstrap-fileinput/css/fileinput.css" rel="stylesheet">
<link href="//cdn.bootcss.com/select2/4.0.3/css/select2.css" rel="stylesheet">
<script>
	var _baseurl = "http://127.0.0.1";
</script>

<style>
.ellipsis {
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
}

.table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
	border-bottom-width: 1px;
}

.pagination {
	margin: 0;
}

.ztree li span.button.add {
	margin-left: 2px;
	margin-right: -1px;
	background-position: -144px 0;
	vertical-align: top;
	*vertical-align: middle
}
</style>
</head>

<body class="gray-bg">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>书签管理</h2>
			<ol class="breadcrumb">
				<li><a href="index.html">主页</a></li>
				<li><a>书签管理</a></li>
			</ol>
		</div>
		<div class="col-lg-2"></div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>书签管理</h5>
					</div>
					<div class="ibox-content">
						<div class="btn-group" role="group">
							<p>
							<button type="button" class="btn btn-success" data-toggle="modal"
								data-target="#importMark">
								<i class="fa fa-plus" aria-hidden="true"></i> 从html导入书签
							</button>
							<button type="button" class="btn btn-success" data-toggle="modal"
								id="repeatViewButton">
								<i class="fa fa-search" aria-hidden="true"></i> 查看重复分类
							</button>
							<button type="button" class="btn btn-success" data-toggle="modal"
								id="repeatMarkViewButton">
								<i class="fa fa-search" aria-hidden="true"></i> 查看重复书签
							</button>
							<button type="button" class="btn btn-success" data-toggle="modal"
								id="relChartViewButton">
								<i class="fa fa-search" aria-hidden="true"></i> 查看关系图
							</button>
							</p>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<div class="col-md-9">
										<input type="text" class="form-control" name="categoryName"
											id="categoryName" placeholder="请输入分类名称" />
									</div>
									<div class="col-md-3">
										<button type="button" id="searchCategory" class="btn btn-info">搜索</button>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<ul id="treeDemo" class="ztree"></ul>
									</div>
								</div>
							</div>
							<div id="main" class="col-md-9">
								<div class="form-horizontal" role="form">
									<div class="form-group">
										<div class="col-md-9">
											<input type="text" name="markName" id="markName"
												placeholder="请输入查询信息" class="form-control" />
										</div>
										<div class="col-md-3">
											<button type="button" id="searchBy" class="btn">搜索</button>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<table id="markData"
												class="table table-striped table-bordered">
												<thead>
													<tr>
														<th width="75%">名称</th>
														<th width="25%">所属分类</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colspan="2">未查询到结果</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7">
											<div id="bootstrapPaginator"></div>
										</div>
										<div class="col-md-5">
											<label class="col-md-3 control-label">显示数量</label>
											<div class="col-md-9">
												<select id="tpageNumber" class="form-control">
													<option value="20">20条</option>
													<option value="50">50条</option>
													<option value="100">100条</option>
													<option value="200">200条</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<#include "/category/upload.ftl" />
	<div class="modal fade" id="repeatCategory" tabindex="-1" role="dialog"
		aria-hidden="true"></div>
	<div class="modal fade" id="repeatMark" tabindex="-1" role="dialog"
		aria-hidden="true"></div>
	<div class="modal fade" id="readMark" tabindex="-1" role="dialog"
		aria-hidden="true"></div>
</body>

<script type="text/javascript"
	src="http://apps.bdimg.com/libs/jquery/1.9.1/jquery.js"></script>
<script
	src="//cdn.bootcss.com/zTree.v3/3.5.24/js/jquery.ztree.all.min.js"></script>
<script src="/js/plugins/bootstrap-paginator.js"></script>
<script src="/js/plugins/eWin.js"></script>

<script src="/js/plugins/bootstrap-fileinput/js/fileinput.js"></script>
<script src="/js/plugins/bootstrap-fileinput/js/locales/zh.js"></script>

<script src="//cdn.bootcss.com/select2/4.0.3/js/select2.full.js"></script>
<script src="//cdn.bootcss.com/select2/4.0.3/js/i18n/zh-CN.js"></script>

<script src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="/js/pages/bookmark/index.js"></script>

<!-- 第三方插件 -->
<script src="/js/plugins/pace/pace.min.js"></script>

</html>