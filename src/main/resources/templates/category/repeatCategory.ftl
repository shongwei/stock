<div class="modal-dialog" style="width:70% !important;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">重复的分类</h4>
		</div>
		<div class="modal-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="25%">名称</th>
						<th width="15%">所属分类</th>
						<th colspan="2" width="20%" style="text-align: center;">操作</th>
					</tr>
				</thead>
				<tbody>
					<#if repeatCategorys??>
						<#list repeatCategorys?keys as key>
							<#list repeatCategorys[key] as category>
								<tr id="${key}_${category_index}">
									<td>${category.title}</td>
									<td>${(category.parentTitle)!''}</td>
									<td><input type="radio" name="${key}_radio" value="${category.id}" /></td>
									<#if category_index==0>
										<td rowspan="${repeatCategorys[key]?size}" style="text-align: center;vertical-align:middle;">
											<button class="btn btn-info" type="button" onclick="mergeC('${key}_radio',this,'${key}',${repeatCategorys[key]?size});">合并</button>
										</td>
									</#if>
								</tr>
							</#list>
							<tr id="${key}_null">
								<td colspan="4">&nbsp;</td>
							</tr>
						</#list>
						<#else>
							<tr>
								<td colspan="4">未查询到结果</td>
							</tr>
					</#if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer ">
			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>